package mailinglist

import (
	"net/http"
	"encoding/hex"
	"crypto/rand"
	"strconv"
	"fmt"
	"time"
	"regexp"
	"database/sql"
)

type mailinglist struct {
	ID string
	Email string
	User bool
	Created int
}

var db *sql.DB

const dbName = "public.mailinglist"

var emailRegexp = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

// SetDB passes db connection
func SetDB(d *sql.DB) {db = d}

// HTTPRouter routes requests
func HTTPRouter(w http.ResponseWriter, r *http.Request) {
	if option := httpCORS(w, r); option {return}

	switch r.Method {
	case http.MethodPost:
		add(w, r)
		break
	case http.MethodDelete:
		delete(w, r)
	}
}

func add(w http.ResponseWriter, r *http.Request) {
	prfx := "failed to add email: %s, email: %s err: %s"

	ml := mailinglist{Email: r.URL.Query().Get("e")}
	if err := isValid(ml.Email); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if err := ml.insertToDB(); err != nil {
		fmt.Printf(prfx, "db insert err", ml.Email, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusCreated)
}

func delete(w http.ResponseWriter, r *http.Request) {
	prfx := "failed to delete subsc: %s, id: %s, err: %s"

	id := r.URL.Query().Get("id")
	if _, err := strconv.ParseUint(id, 16, 64); err != nil {
		fmt.Printf(prfx, "invalid id", id, err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	_, err := db.Query("DELETE FROM "+dbName+" WHERE id = $1", id)
	if err != nil {
		fmt.Printf(prfx, "invalid id", id, err)
	}

	w.Header().Add("Content-type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)
}

func (ml *mailinglist) insertToDB() error {
	id, err := generateID(10)
	if err != nil {
		return fmt.Errorf("id gen err: %s", err)
	}
	ml.ID = id

	ml.Created = int(time.Now().Unix())

	_, err = db.Query("INSERT INTO "+dbName+" (id, email, is_user, created) "+
		"VALUES ($1, $2, $3, $4)", ml.ID, ml.Email, ml.User, ml.Created)

	return err
}

func isValid(e string) error {
	if !emailRegexp.MatchString(e) {
		return fmt.Errorf("Oops, email not valid format")
	}
	return nil
}

func generateID(len int) (string, error) {
	b := make([]byte, int(len/2))
	if _, err := rand.Read(b); err != nil {
		return "", err
	}
	return hex.EncodeToString(b), nil
}

func httpCORS(w http.ResponseWriter, r *http.Request) bool {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	if r.Method == http.MethodOptions {
		w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		w.Header().Set("Access-Control-Max-Age", "3600")
		w.WriteHeader(http.StatusOK)
		return true;
	}

	return false
}