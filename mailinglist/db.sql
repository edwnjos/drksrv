CREATE TABLE public.mailinglist
(
    id character varying(12) COLLATE pg_catalog."default" NOT NULL,
    email text COLLATE pg_catalog."default" NOT NULL,
    is_user boolean NOT NULL,
    created integer NOT NULL,
    CONSTRAINT mailinglist_pkey PRIMARY KEY (id),
    CONSTRAINT mailinglist_uemail UNIQUE (email)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.mailinglist
    OWNER to postgres;
