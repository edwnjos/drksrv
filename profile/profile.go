package profile

import (
	"net/http"
	"time"
	"io/ioutil"
	"encoding/json"
	"regexp"
	"encoding/hex"
	"crypto/rand"
	"fmt"
	"unicode"
	"errors"
	"database/sql"
)

type profile struct {
	ID      string `json:"ID,omitempty"`
	Name    string `json:"Name,omitempty"`
	Email   string `json:"Email,omitempty"`
	Gender  string `json:"Gender,omitempty"`
	DOB     string `json:"DOB,omitempty"`
	Updated int64  `json:"Updated,omitempty"`
	Created int64  `json:"Created,omitempty"`
}

var db *sql.DB

const dbName = "public.profiles"

// SetDB passes db connection
func SetDB(d *sql.DB) {db = d}

var emailRegexp = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

// HTTPRouter routes requests
func HTTPRouter(w http.ResponseWriter, r *http.Request) {
	if option := httpCORS(w, r); option {return}

	switch r.Method {
	case http.MethodGet:
		getProfile(w, r)
		break
	case http.MethodPost:
		newProfile(w, r)
		break
	}
}

func newProfile(w http.ResponseWriter, r *http.Request) {
	prfx := "\nfailed to create profile: %s, err: %#v\n"

	r.Body = http.MaxBytesReader(w, r.Body, 180)

	var p map[string]string
	if err := json.NewDecoder(r.Body).Decode(&p); err != nil {
		fmt.Printf(prfx, "json decoding err", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	pr := profile{Name: p["name"], Email: p["email"]}
	if err := pr.isValid(); err != nil {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte(err.Error()))
		return
	}

	exists, err := exists(pr.Email)
	if err != nil {
		fmt.Printf(prfx, "exists check err", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if exists {
		w.WriteHeader(http.StatusConflict)
		return
	}

	if err := pr.insertToDB(); err != nil {
		fmt.Printf(prfx, "db insert err", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)
}

func getProfile(w http.ResponseWriter, r *http.Request) {
	prfx := "\nfailed to get profile: %s, err: %#v\n"

	id, err := authorize(w, r.Header.Get("Authorization"))
	if err != nil {
		fmt.Printf(prfx, "failed to auth", err.Error())
		return
	}

	pr, err := getFromDB(id)
	if err != nil {
		fmt.Printf(prfx, "db get err", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(pr); err != nil {
		fmt.Printf(prfx, "json encoding err", err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func httpCORS(w http.ResponseWriter, r *http.Request) bool {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	if r.Method == http.MethodOptions {
		w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		w.Header().Set("Access-Control-Max-Age", "3600")
		w.WriteHeader(http.StatusOK)
		return true;
	}

	return false
}

// GetID fetches user_id using email
func GetID(e string) (string, error) {
	var id string

	err := db.QueryRow("SELECT id FROM "+dbName+" WHERE email = $1", e).Scan(&id)
	if err != nil {
		if err.Error() == "sql: no rows in result set" {
			return id, errors.New("not_found")
		}
	}
	
	return id, err
}

func (pr *profile) insertToDB() error {
	id, err := generateID(12)
	if err != nil {
		return fmt.Errorf("id gen err: %s", err)
	}
	pr.ID = id

	pr.Updated = time.Now().Unix()

	pr.Created = time.Now().Unix()

	_, err = db.Query("INSERT INTO "+dbName+" (id, name, email, gender, "+
		"dob, updated, created) VALUES ($1, $2, $3, $4, $5, $6, $7)", pr.ID, 
		pr.Name, pr.Email, pr.Gender, pr.DOB, pr.Updated, pr.Created)

	return err
}

func getFromDB(id string) (profile, error) {
	row := db.QueryRow("SELECT name, email, gender, dob, updated,"+ 
		"created FROM "+dbName+" WHERE id = $1", id)

	pr := profile{ID: id}
	err := row.Scan(&pr.Name, &pr.Email, &pr.Gender, &pr.DOB, &pr.Updated, &pr.Created)

	return pr, err
}

func exists(e string) (bool, error) {
	if _, err := GetID(e); err != nil {
		if err.Error() == "not_found" {
			return false, nil
		}
		return true, err
	}
	return true, nil
}

func authorize(w http.ResponseWriter, tkn string) (string, error) {
	req, err := http.NewRequest("GET", "http://localhost:8080/auth", nil)
	if err != nil {
		return "", fmt.Errorf("failed to create req: %s", err.Error())
	}
	req.Header.Set("Authorization", tkn)

	res, err := (&http.Client{}).Do(req)
	if err != nil {
		return "", fmt.Errorf("failed to execute req: %s", err.Error())
	}

	switch res.StatusCode {
	case 200:
		break
	case 403:
		w.WriteHeader(http.StatusForbidden)
		return "", fmt.Errorf("req returned status: 403")
	default:
		w.WriteHeader(http.StatusInternalServerError)
		return "", fmt.Errorf("req returned status: %d", res.StatusCode)
	}

	userID, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", fmt.Errorf("failed to read body: %s", err.Error())
	}

	return string(userID), nil
}

func (pr *profile) isValid() error {
	if err := isNameValid(pr.Name); err != nil {
		return err
	}
	if !emailRegexp.MatchString(pr.Email) {
		return fmt.Errorf("Oops, email not valid format")
	}
	return nil
}

func isNameValid(n string) error {
	if len(n) < 2 {
		return fmt.Errorf("Oops, name cannot be less 2 charecters long")
	}
	if len(n) > 50 {
		return fmt.Errorf("Oops, name cannot be longer than 50 charecters")
	}
	for _, letter := range n {
		if unicode.IsNumber(letter) {
			return fmt.Errorf("Oops, name cannot have digits")
		}
	}
	for _, letter := range n {
		if unicode.IsSymbol(letter) {
			return fmt.Errorf("Oops, name cannot have symbols")
		}
	}
	return nil
}

func isDOBValid(dob string) error {
	if len(dob) != 8 {
		return fmt.Errorf("Oops, date of birth not valid")
	}
	t, err := time.Parse("02/01/2006", dob[:2]+"/"+dob[2:4]+"/"+dob[4:8])
	if err != nil {
		return fmt.Errorf("Oops, date of birth not valid")
	}
	if t.Unix() > time.Now().AddDate(-18, 0, 0).Unix() {
		return fmt.Errorf("Oops, you need to be atleast 18 years old")
	}
	return nil
}

func isGenderValid(g string) error {
	switch g {
	case "", "male", "female":
		return nil
	}
	return fmt.Errorf("Oops, gender value not valid")
}

func generateID(len int) (string, error) {
	b := make([]byte, int(len/2))
	if _, err := rand.Read(b); err != nil {
		return "", err
	}
	return hex.EncodeToString(b), nil
}