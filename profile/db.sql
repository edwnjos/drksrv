CREATE TABLE public.profiles
(
    id character varying(12) COLLATE pg_catalog."default" NOT NULL,
    name character varying(64) COLLATE pg_catalog."default",
    email text COLLATE pg_catalog."default" NOT NULL,
    gender character varying(12) COLLATE pg_catalog."default",
    dob character varying(8) COLLATE pg_catalog."default",
    updated integer NOT NULL,
    created integer NOT NULL,
    CONSTRAINT profiles_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.profiles
    OWNER to postgres;