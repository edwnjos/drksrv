module gitlab.com/edwnjos/drksrv

go 1.13

require (
	github.com/edwnjos/mail v0.0.0-20190407141349-3c92cddbde37 // indirect
	github.com/lib/pq v1.2.0
	gitlab.com/edwnjos/drksrv/analytics v0.0.0-20200111214607-cc9e5521f845
	gitlab.com/edwnjos/drksrv/auth v0.0.0-20191110151846-7ef3b977abf1
	gitlab.com/edwnjos/drksrv/mailinglist v0.0.0-20200121142044-38eb215781e9
	gitlab.com/edwnjos/drksrv/plans v0.0.0-20200105223536-b2d4f2e72514
	gitlab.com/edwnjos/drksrv/profile v0.0.0-20191110145308-f52c9aab050e
	gitlab.com/edwnjos/drksrv/workoutlogs v0.0.0-20200131124136-109d11c5c734
	gitlab.com/edwnjos/drksrv/workouts v0.0.0-20200105223536-b2d4f2e72514
)

replace gitlab.com/edwnjos/drksrv/auth => ./auth

replace gitlab.com/edwnjos/drksrv/profile => ./profile

replace gitlab.com/edwnjos/drksrv/plans => ./plans

replace gitlab.com/edwnjos/drksrv/workouts => ./workouts

replace gitlab.com/edwnjos/drksrv/analytics => ./analytics

replace gitlab.com/edwnjos/drksrv/mailinglist => ./mailinglist

replace gitlab.com/edwnjos/drksrv/workoutlogs => ./workoutlogs
