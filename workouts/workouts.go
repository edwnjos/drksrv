package workouts

import (
	"net/http"
	"strings"
	
	"errors"
	"time"
	"encoding/json"
	"net/url"
	"io/ioutil"
	"database/sql"
	"strconv"
	"fmt"
)

type workout struct {
	ID string;
	UserID string
	PlanID string;
	Name string;
	Sets int;
	Reps int
	Weight int;
	Updated int;
	Created int;
}

var db *sql.DB

const dbName = "public.workouts"

// SetDB passes db connection
func SetDB(d *sql.DB) {db = d}

// HTTPRouter routes requests
func HTTPRouter(w http.ResponseWriter, r *http.Request) {
	if option := httpCORS(w, r); option {return}

	switch r.Method {
	case http.MethodPost:
		add(w, r)
		break
	case http.MethodPut:
		edit(w, r)
	case http.MethodGet:
		get(w, r)
		break
	case http.MethodDelete:
		delete(w, r)
	}
}

func add(w http.ResponseWriter, r *http.Request) {
	prfx := "\nfailed to add workout: %s, w: %#v, err: %s"

	r.Body = http.MaxBytesReader(w, r.Body, 512)

	tkn := r.Header.Get("Authorization");

	userID, err := authorize(w, tkn)
	if err != nil {
		fmt.Printf(prfx, "failed to auth", nil, err.Error())
		return
	}

	var wk workout
	if err := json.NewDecoder(r.Body).Decode(&wk); err != nil {
		fmt.Printf(prfx, "json decoding err", wk, err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	wk.UserID = userID;

	if err := wk.isValid(); err != nil {
		fmt.Printf(prfx, "validation err", wk, err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	p, err := getPlan(wk.PlanID, tkn);
	if err != nil {
		fmt.Printf(prfx, "get plan err", wk, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if p["UserID"].(string) != wk.UserID {
		fmt.Printf(prfx, "non owner add", wk, "owner_id: "+p["UserID"].(string))
		w.WriteHeader(http.StatusInternalServerError)
		return	
	}

	if err := wk.insertToDB(); err != nil {
		fmt.Printf(prfx, "db insert err", wk, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusCreated)
}

func edit(w http.ResponseWriter, r *http.Request) {
	prfx := "\nfailed to edit workout: %s, v: %#v, err: %s"

	r.Body = http.MaxBytesReader(w, r.Body, 512)

	tkn := r.Header.Get("Authorization");

	userID, err := authorize(w, tkn)
	if err != nil {
		fmt.Printf(prfx, "failed to auth", nil, err.Error())
		return
	}

	id, f, v := r.URL.Query().Get("id"), r.URL.Query().Get("f"),
		r.URL.Query().Get("v")
	if err := isFieldValid(f, v); err != nil {
		fmt.Printf(prfx, "validation err", v, err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	
	if err := updateDB(id, userID, f, v); err != nil {
		fmt.Printf(prfx, "db update err", v, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)
}

func get(w http.ResponseWriter, r *http.Request) {
	prfx := "\nfailed to get workout: %s, wk: %#v, err: %s"

	r.Body = http.MaxBytesReader(w, r.Body, 32)

	planID := r.URL.Query().Get("plan_id")
	userID, err := authorize(w, r.Header.Get("Authorization"))
	if err != nil {
		fmt.Printf(prfx, "failed to auth", nil, err.Error())
		return
	}

	if (r.URL.Query().Get("ids") != "") {
		syncQuery(w, r, userID, planID)
		return
	}

	if planID == "" {
		initQuery(w, r, userID)
		return
	}

	wrks, err := getFromDB(planID)
	if err != nil {
		if err == sql.ErrNoRows {
			w.WriteHeader(http.StatusNoContent)
			return 
		}
		fmt.Printf(prfx, "db get err", wrks, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if len(wrks) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	if wrks[0].UserID != userID {
		fmt.Printf(prfx, "non owner query", wrks, "user_id: "+userID)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(wrks); err != nil {
		fmt.Printf(prfx, "json encoding err", wrks, err)
		w.WriteHeader(http.StatusInternalServerError)
	}	
}

func initQuery(w http.ResponseWriter, r *http.Request, userID string) {
	prfx := "\nfailed to query plans: %s, userId: %#v, err: %s"

	workouts, err := initQueryDB(userID)
	if err != nil {
		fmt.Printf(prfx, "db get err", userID, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	
	if len(workouts) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return 
	}
	
	w.Header().Add("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(workouts); err != nil {
		fmt.Printf(prfx, "json encoding err", userID, err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func syncQuery(w http.ResponseWriter, r *http.Request, userID string, planID string) {
	prfx := "\nfailed to query plans: %s, userId: %#v, err: %s"

	workouts, err := queryDB(r.URL.Query().Get("ids"), planID, userID)
	if err != nil {
		fmt.Printf(prfx, "db get err", userID, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	
	if len(workouts) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return 
	}
	
	w.Header().Add("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(workouts); err != nil {
		fmt.Printf(prfx, "json encoding err", userID, err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func delete(w http.ResponseWriter, r *http.Request) {
	prfx := "\nfailed to delete workout: %s, id: %s, err: %s"

	r.Body = http.MaxBytesReader(w, r.Body, 32)

	id := r.URL.Query().Get("id")
	if id == "" {
		deletePlanWorkouts(w, r);
		return
	}

	userID, err := authorize(w, r.Header.Get("Authorization"))
	if err != nil {
		fmt.Printf(prfx, "failed to auth", id, err.Error())
		return
	}

	if err := removeFromDB(id, userID); err != nil {
		fmt.Printf(prfx, "failed to remove workot from db", id, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)
}

func deletePlanWorkouts(w http.ResponseWriter, r *http.Request) {
	prfx := "\nfailed to delete workout: %s, plan_id: %s, err: %s"

	r.Body = http.MaxBytesReader(w, r.Body, 32)

	userID, err := authorize(w, r.Header.Get("Authorization"))
	if err != nil {
		fmt.Printf(prfx, "failed to auth", nil, err.Error())
		return
	}

	planID := r.URL.Query().Get("plan_id")
	if planID == "" {
		fmt.Printf(prfx, "plan id empty", planID, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := removePlanWFromDB(planID, userID); err != nil {
		fmt.Printf(prfx, "failed to remove plan workouts from db", planID, err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)
}

func (wk *workout) isValid() error {
	if len(wk.ID) > 12 || len(wk.UserID) > 12 || len(wk.PlanID) > 12 {
		return errors.New("ids len greater than 12")
	}

	if err := isNameValid(wk.Name); err != nil {
		return err
	}

	if err := isSetsValid(wk.Sets); err != nil {
		return err
	}

	if err := isRepsValid(wk.Reps); err != nil {
		return err
	}

	if err := isWeightValid(wk.Weight); err != nil {
		return err
	}

	var n = int(time.Now().Unix())
	if wk.Updated > n || wk.Created > n  {
		return errors.New("invalid updated/created dates")
	}

	return nil
}

func isFieldValid(f string, v interface{}) error {
	if f != "name" {
		vint, err := strconv.Atoi(v.(string))
		if err != nil {
			return fmt.Errorf("string to int conv failed: %s", err.Error())
		}
		v = vint
	}

	switch f {
	case "name":
		if err := isNameValid(v.(string)); err != nil {
			return err
		}
		break
	case "sets":
		if err := isSetsValid(v.(int)); err != nil {
			return err
		}
		break
	case "reps":
		if err := isRepsValid(v.(int)); err != nil {
			return err
		}
		break
	case "weight":
		if err := isWeightValid(v.(int)); err != nil {
			return err
		}
		break
	default:
		return fmt.Errorf("invalid field: %s", f)
	}

	return nil
}

func isNameValid(n string) error {
	if len(n) > 100 {
		return errors.New("name len greater than 100")
	}
	return nil
}

func isSetsValid(s int) error {
	if s < 0 || s > 100000 {
		return errors.New("sets too long or short")
	}
	return nil
}

func isRepsValid(r int) error {
	if r < 0 || r > 100000 {
		return errors.New("reps too long or short")
	}
	return nil
}

func isWeightValid(w int) error {
	if w < 0 || w > 100000 {
		return errors.New("weight too long or short")
	}
	return nil
}

func initQueryDB(userID string) ([]workout, error) {
	workouts := []workout{}

	q := "SELECT id, user_id, plan_id, name, sets, reps, weight, updated, "+ 
		"created FROM "+dbName+" WHERE user_id=$1"

	rows, err := db.Query(q, userID)
	if err != nil {
		return workouts, err
	}
	defer rows.Close()

	for rows.Next() {
		wk := workout{}
		err := rows.Scan(&wk.ID, &wk.UserID, &wk.PlanID, &wk.Name, &wk.Sets, 
			&wk.Reps, &wk.Weight, &wk.Updated, &wk.Created)
		if err != nil {
			return workouts, err
		}

		workouts = append(workouts, wk)
	}
	if err := rows.Err(); err != nil {
		return workouts, err;
	}

	return workouts, err
}

func queryDB(ids, planID, userID string) ([]workout, error) {
	workouts := []workout{}

	idsList := []interface{}{userID, planID}
	for _, v := range strings.Split(ids, ",") {
		idsList = append(idsList, v)
	}

	params := ""
	for i:=3;i<len(idsList)+1;i++ {
		params = params+"$"+strconv.Itoa(i)+","
	}

	params = strings.TrimSuffix(params, ",")

	q := "SELECT id, user_id, name, sets, reps, weight, updated, "+ 
		"created FROM "+dbName+" WHERE user_id=$1 AND plan_id=$2 AND id NOT IN ("+params+")"

	rows, err := db.Query(q, idsList...)
	if err != nil {
		return workouts, err
	}
	defer rows.Close()

	for rows.Next() {
		wk := workout{PlanID: planID}
		err := rows.Scan(&wk.ID, &wk.UserID, &wk.Name, &wk.Sets, 
			&wk.Reps, &wk.Weight, &wk.Updated, &wk.Created)
		if err != nil {
			return workouts, err
		}

		workouts = append(workouts, wk)
	}
	if err := rows.Err(); err != nil {
		return workouts, err;
	}

	return workouts, err
}


func getFromDB(planID string) ([]workout, error) {
	wrks := []workout{}

	q := "SELECT id, user_id, name, sets, reps, weight, updated, "+
	"created FROM "+dbName+" WHERE plan_id = $1"

	rows, err := db.Query(q, planID)
	if err != nil {
		return wrks, err
	}
	defer rows.Close()

	for rows.Next() {
		wk := workout{PlanID: planID}
		err := rows.Scan(&wk.ID, &wk.UserID, &wk.Name, &wk.Sets, 
			&wk.Reps, &wk.Weight, &wk.Updated, &wk.Created)
		if err != nil {
			return wrks, err
		}

		wrks = append(wrks, wk)
	}
	if err := rows.Err(); err != nil {
		return wrks, err;
	}

	return wrks, err
}

func (wk *workout) insertToDB() error {
	_, err := db.Query("INSERT INTO "+dbName+" (id, user_id, plan_id, name, sets,"+
		"reps, weight, updated, created) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9)", 
		wk.ID, wk.UserID, wk.PlanID, wk.Name, wk.Sets, wk.Reps, wk.Weight, wk.Updated, wk.Created)
	return err
}

func updateDB(id, userID, f string, v interface{}) error {
	_, err := db.Query("UPDATE "+dbName+" SET "+f+"=$1 WHERE id=$2 AND user_id=$3", 
		v, id, userID)
	return err
}

func removeFromDB(id, userID string) error {
	_, err := db.Query("DELETE FROM "+dbName+" WHERE id=$1 AND user_id=$2", id, userID)
	return err
}

func removePlanWFromDB(planID, userID string) error {
	_, err := db.Query("DELETE FROM "+dbName+" WHERE plan_id=$1 AND user_id=$2", planID, userID)
	return err
}

func authorize(w http.ResponseWriter, tkn string) (string, error) {
	req, err := http.NewRequest("GET", "http://localhost:8080/auth", nil)
	if err != nil {
		return "", fmt.Errorf("failed to create req: %s", err.Error())
	}
	req.Header.Set("Authorization", tkn)

	res, err := (&http.Client{}).Do(req)
	if err != nil {
		return "", fmt.Errorf("failed to execute req: %s", err.Error())
	}

	switch res.StatusCode {
	case 200:
		break
	case 403:
		w.WriteHeader(http.StatusForbidden)
		return "", fmt.Errorf("req returned status: 403")
	default:
		w.WriteHeader(http.StatusInternalServerError)
		return "", fmt.Errorf("req returned status: %d", res.StatusCode)
	}
	
	userID, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", fmt.Errorf("failed to read body: %s", err.Error())
	}

	return string(userID), nil
}

func getPlan(id string, tkn string) (map[string]interface{}, error) {
	req, err := http.NewRequest("GET", "http://localhost:8080/plans", nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create req: %s", err.Error())
	}
	req.Header.Set("Authorization", tkn)

	req.URL.RawQuery = url.Values{"id": []string{id}}.Encode()

	res, err := (&http.Client{}).Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to execute req: %s", err.Error())
	}

	switch res.StatusCode {
	case 200:
		break
	default:
		return nil, fmt.Errorf("req status: %d", res.StatusCode)
	}
	
	var p map[string]interface{}
	if err := json.NewDecoder(res.Body).Decode(&p); err != nil {
		return nil, fmt.Errorf("req body decoding failed: %s", err.Error())
	}

	return p, nil
}

func httpCORS(w http.ResponseWriter, r *http.Request) bool {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	if r.Method == http.MethodOptions {
		w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		w.Header().Set("Access-Control-Max-Age", "3600")
		w.WriteHeader(http.StatusOK)
		return true;
	}

	return false
}