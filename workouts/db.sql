CREATE TABLE public.workouts
(
    id character varying(12) COLLATE pg_catalog."default" NOT NULL,
    user_id character varying(12) COLLATE pg_catalog."default" NOT NULL,
    plan_id character varying(12) COLLATE pg_catalog."default" NOT NULL,
    name text COLLATE pg_catalog."default" NOT NULL,
    reps integer,
    sets integer,
    weight integer,
    updated integer NOT NULL,
    created integer NOT NULL,
    CONSTRAINT workouts_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.workouts
    OWNER to postgres;
