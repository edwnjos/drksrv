package workoutlogs

import (
	"database/sql"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

type workoutlog struct {
	ID        string
	UserID    string
	PlanID    string
	WorkoutID string
	Reps      int
	Weight    float32
	Created   int
}

var db *sql.DB

const dbName = "public.workout_logs"

// SetDB passes db connection
func SetDB(d *sql.DB) { db = d }

// HTTPRouter routes requests
func HTTPRouter(w http.ResponseWriter, r *http.Request) {
	if option := httpCORS(w, r); option {
		return
	}

	switch r.Method {
	case http.MethodPost:
		add(w, r)
		break
	case http.MethodGet:
		get(w, r)
		break
	case http.MethodPut:
		edit(w, r)
	case http.MethodDelete:
		remove(w, r)
	}
}

func add(w http.ResponseWriter, r *http.Request) {
	prfx := "\nfailed to add workoutlog: %s, wl: %#v, err: %s"

	r.Body = http.MaxBytesReader(w, r.Body, 256)

	tkn := r.Header.Get("Authorization")

	userID, err := authorize(w, tkn)
	if err != nil {
		fmt.Printf(prfx, "failed to auth", err.Error())
		return
	}

	var wl workoutlog
	if err := json.NewDecoder(r.Body).Decode(&wl); err != nil {
		fmt.Printf(prfx, "json decoding err", wl, err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	wl.UserID = userID

	if err := wl.isValid(); err != nil {
		fmt.Printf(prfx, "validation err", wl, err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	p, err := getPlan(wl.PlanID, tkn)
	if err != nil {
		fmt.Printf(prfx, "get plan err", wl, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	if p["UserID"].(string) != wl.UserID {
		fmt.Printf(prfx, "non owner add", wl, "owner_id: "+p["UserID"].(string))
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := wl.insertToDB(); err != nil {
		fmt.Printf(prfx, "db insert err", wl, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusCreated)
}

func edit(w http.ResponseWriter, r *http.Request) {
	prfx := "\nfailed to edit plan: %s, p: %#v, err: %s"

	r.Body = http.MaxBytesReader(w, r.Body, 256)

	userID, err := authorize(w, r.Header.Get("Authorization"))
	if err != nil {
		fmt.Printf(prfx, "failed to auth", nil, err.Error())
		return
	}

	id, f, v := r.URL.Query().Get("id"), r.URL.Query().Get("f"),
		r.URL.Query().Get("v")
	if err := isFieldValid(f, v); err != nil {
		fmt.Printf(prfx, "validation err", v, err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if err := updateDB(id, userID, f, v); err != nil {
		fmt.Printf(prfx, "db update err", v, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)
}

func get(w http.ResponseWriter, r *http.Request) {
	prfx := "\nfailed to get plan: %s, p: %#v, err: %s"

	r.Body = http.MaxBytesReader(w, r.Body, 32)

	userID, err := authorize(w, r.Header.Get("Authorization"))
	if err != nil {
		fmt.Printf(prfx, "failed to auth", nil, err.Error())
		return
	}

	planID, workoutID := r.URL.Query().Get("plan_id"),
		r.URL.Query().Get("workout_id")

	if planID == "" && workoutID == "" {
		initQuery(w, r, userID)
		return
	}

	if ids := r.URL.Query().Get("ids"); len(ids) > 0 {
		syncQuery(w, r, userID, planID, ids)
		return
	}

	wlogs, err := getFromDB(userID, planID, workoutID)
	if err != nil {
		if err == sql.ErrNoRows {
			w.WriteHeader(http.StatusNoContent)
			return
		}
		fmt.Printf(prfx, "db get err", wlogs, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if len(wlogs) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	w.Header().Add("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(wlogs); err != nil {
		fmt.Printf(prfx, "json encoding err", wlogs, err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func initQuery(w http.ResponseWriter, r *http.Request, userID string) {
	prfx := "\nfailed to init query workoutlogs: %s, userId: %#v, err: %s"

	workoutlogs, err := initQueryDB(userID)
	if err != nil {
		fmt.Printf(prfx, "db get err", userID, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if len(workoutlogs) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	w.Header().Add("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(workoutlogs); err != nil {
		fmt.Printf(prfx, "json encoding err", userID, err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func syncQuery(w http.ResponseWriter, r *http.Request, userID, planID, ids string) {
	prfx := "\nfailed to sync query workoutlogs: %s, userId: %#v, err: %s"

	workoutlogs, err := syncQueryDB(ids, userID, planID)
	if err != nil {
		fmt.Printf(prfx, "db get err", userID, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if len(workoutlogs) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return
	}

	w.Header().Add("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(workoutlogs); err != nil {
		fmt.Printf(prfx, "json encoding err", userID, err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func remove(w http.ResponseWriter, r *http.Request) {
	prfx := "\nfailed to remove workoutlog: %s, id: %s, err: %s"

	r.Body = http.MaxBytesReader(w, r.Body, 32)

	userID, err := authorize(w, r.Header.Get("Authorization"))
	if err != nil {
		fmt.Printf(prfx, "failed to auth", "", err.Error())
		return
	}

	id := r.URL.Query().Get("id")
	if err := removeFromDB(id, userID); err != nil {
		fmt.Printf(prfx, "db delete err", id, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)
}

func isFieldValid(f string, v interface{}) error {
	switch f {
	case "reps":
		vint, err := strconv.Atoi(v.(string))
		if err != nil {
			return fmt.Errorf("string to int conv failed: %s", err.Error())
		}
		v = vint

		if err := isRepsValid(v.(int)); err != nil {
			return err
		}
		break
	case "weight":
		vfl, err := strconv.ParseFloat(v.(string), 32)
		if err != nil {
			return fmt.Errorf("string to int conv failed: %s", err.Error())
		}
		v = vfl

		if err := isWeightValid(v.(float32)); err != nil {
			return err
		}
		break
	default:
		return fmt.Errorf("invalid field: %s", f)
	}

	return nil
}

func (wl *workoutlog) isValid() error {
	if len(wl.ID) > 12 || len(wl.UserID) > 12 || len(wl.PlanID) > 12 || len(wl.WorkoutID) > 12 {
		return errors.New("ids len greater than 12")
	}

	if err := isRepsValid(wl.Reps); err != nil {
		return err
	}

	if err := isWeightValid(wl.Weight); err != nil {
		return err
	}

	return nil
}

func isRepsValid(r int) error {
	if r < 0 || r > 100000 {
		return errors.New("reps too long or short")
	}
	return nil
}

func isWeightValid(w float32) error {
	if w < 0 || w > 100000 {
		return errors.New("weight too long or short")
	}
	return nil
}

func (wl *workoutlog) insertToDB() error {
	_, err := db.Query("INSERT INTO "+dbName+" (id, user_id, plan_id, workout_id, "+
		"reps, weight, created) VALUES ($1, $2, $3, $4, $5, $6, $7)",
		wl.ID, wl.UserID, wl.PlanID, wl.WorkoutID, wl.Reps, wl.Weight, wl.Created)
	return err
}

func updateDB(id, userID, f string, v interface{}) error {
	_, err := db.Query("UPDATE "+dbName+" SET "+f+"=$1 WHERE id=$2 AND user_id=$3",
		v, id, userID)
	return err
}

func removeFromDB(id, userID string) error {
	_, err := db.Query("DELETE FROM "+dbName+" WHERE id=$1 AND user_id=$2", id, userID)
	return err
}

func syncQueryDB(ids, userID, planID string) ([]workoutlog, error) {
	wlogs := []workoutlog{}

	idsList := []interface{}{userID, planID}
	for _, v := range strings.Split(ids, ",") {
		idsList = append(idsList, v)
	}

	params := ""
	for i := 3; i < len(idsList)+1; i++ {
		params = params + "$" + strconv.Itoa(i) + ","
	}

	params = strings.TrimSuffix(params, ",")

	q := "SELECT id, workout_id, reps, weight, created FROM " + dbName +
		" WHERE user_id=$1 AND plan_id=$2 AND id NOT IN (" + params + ")"

	rows, err := db.Query(q, idsList...)
	if err != nil {
		return wlogs, err
	}
	defer rows.Close()

	for rows.Next() {
		wl := workoutlog{UserID: userID, PlanID: planID}
		err := rows.Scan(&wl.ID, &wl.WorkoutID, &wl.Reps, &wl.Weight, &wl.Created)
		if err != nil {
			return wlogs, err
		}

		wlogs = append(wlogs, wl)
	}
	if err := rows.Err(); err != nil {
		return wlogs, err
	}

	return wlogs, err
}

func initQueryDB(userID string) ([]workoutlog, error) {
	wlogs := []workoutlog{}

	q := "SELECT id, plan_id, workout_id, reps, weight, created FROM " + dbName +
		" WHERE user_id=$1"

	rows, err := db.Query(q, userID)
	if err != nil {
		return wlogs, err
	}
	defer rows.Close()

	for rows.Next() {
		wl := workoutlog{UserID: userID}
		err := rows.Scan(&wl.ID, &wl.PlanID, &wl.WorkoutID, &wl.Reps, &wl.Weight, &wl.Created)
		if err != nil {
			return wlogs, err
		}

		wlogs = append(wlogs, wl)
	}
	if err := rows.Err(); err != nil {
		return wlogs, err
	}

	return wlogs, err
}

func getFromDB(userID, planID, workoutID string) ([]workoutlog, error) {
	wlogs := []workoutlog{}

	q := "SELECT id, workout_id, reps, weight, created FROM " + dbName +
		" WHERE user_id=$1 AND plan_id=$2 AND workout_id=$3"

	rows, err := db.Query(q, userID, planID, workoutID)
	if err != nil {
		return wlogs, err
	}
	defer rows.Close()

	for rows.Next() {
		wl := workoutlog{UserID: userID, PlanID: planID, WorkoutID: workoutID}
		err := rows.Scan(&wl.ID, &wl.WorkoutID, &wl.Reps,
			&wl.Weight, &wl.Created)
		if err != nil {
			return wlogs, err
		}

		wlogs = append(wlogs, wl)
	}
	if err := rows.Err(); err != nil {
		return wlogs, err
	}

	return wlogs, err
}

func getPlan(id string, tkn string) (map[string]interface{}, error) {
	req, err := http.NewRequest("GET", "http://localhost:8080/plans", nil)
	if err != nil {
		return nil, fmt.Errorf("failed to create req: %s", err.Error())
	}
	req.Header.Set("Authorization", tkn)

	req.URL.RawQuery = url.Values{"id": []string{id}}.Encode()

	res, err := (&http.Client{}).Do(req)
	if err != nil {
		return nil, fmt.Errorf("failed to execute req: %s", err.Error())
	}

	switch res.StatusCode {
	case 200:
		break
	default:
		return nil, fmt.Errorf("req status: %d", res.StatusCode)
	}

	var p map[string]interface{}
	if err := json.NewDecoder(res.Body).Decode(&p); err != nil {
		return nil, fmt.Errorf("req body decoding failed: %s", err.Error())
	}

	return p, nil
}

func authorize(w http.ResponseWriter, tkn string) (string, error) {
	req, err := http.NewRequest("GET", "http://localhost:8080/auth", nil)
	if err != nil {
		return "", fmt.Errorf("failed to create req: %s", err.Error())
	}
	req.Header.Set("Authorization", tkn)

	res, err := (&http.Client{}).Do(req)
	if err != nil {
		return "", fmt.Errorf("failed to execute req: %s", err.Error())
	}

	switch res.StatusCode {
	case 200:
		break
	case 403:
		w.WriteHeader(http.StatusForbidden)
		return "", fmt.Errorf("req returned status: 403")
	default:
		w.WriteHeader(http.StatusInternalServerError)
		return "", fmt.Errorf("req returned status: %d", res.StatusCode)
	}

	userID, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", fmt.Errorf("failed to read body: %s", err.Error())
	}

	return string(userID), nil
}

func httpCORS(w http.ResponseWriter, r *http.Request) bool {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	if r.Method == http.MethodOptions {
		w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		w.Header().Set("Access-Control-Max-Age", "3600")
		w.WriteHeader(http.StatusOK)
		return true
	}

	return false
}
