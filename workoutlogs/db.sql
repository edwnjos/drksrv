CREATE TABLE public.workout_logs
(
    id character varying(12) COLLATE pg_catalog."default" NOT NULL,
    user_id character varying(12) COLLATE pg_catalog."default" NOT NULL,
    plan_id character varying(12) COLLATE pg_catalog."default" NOT NULL,
    workout_id character varying(12) COLLATE pg_catalog."default" NOT NULL,
    reps integer,
    weight real,
    created integer NOT NULL,
    CONSTRAINT workout_logs_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.workout_logs
    OWNER to postgres;
