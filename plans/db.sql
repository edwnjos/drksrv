CREATE TABLE public.plans
(
    id character varying(12) COLLATE pg_catalog."default" NOT NULL,
    user_id character varying(12) COLLATE pg_catalog."default" NOT NULL,
    title text COLLATE pg_catalog."default" NOT NULL,
    updated integer NOT NULL,
    created integer NOT NULL,
    CONSTRAINT plans_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.plans
    OWNER to postgres;
