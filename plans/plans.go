package plans

import (
	"net/http"
	"errors"
	"time"
	"encoding/json"
	"strings"
	"io/ioutil"
	"strconv"
	"database/sql"
	"fmt"
)

type plan struct {
	ID string
	UserID string
	Title string
	Updated int
	Created int
}

var db *sql.DB

const dbName = "public.plans"

// SetDB passes db connection
func SetDB(d *sql.DB) {db = d}

// HTTPRouter routes requests
func HTTPRouter(w http.ResponseWriter, r *http.Request) {
	if option := httpCORS(w, r); option {return}

	switch r.Method {
	case http.MethodPost:
		add(w, r)
		break
	case http.MethodGet:
		get(w, r)
		break
	case http.MethodPut:
		edit(w, r)
	case http.MethodDelete:
		remove(w, r)
	}
}

func add(w http.ResponseWriter, r *http.Request) {
	prfx := "\nfailed to add plan: %s, p: %#v, err: %s"

	r.Body = http.MaxBytesReader(w, r.Body, 256)

	userID, err := authorize(w, r.Header.Get("Authorization"))
	if err != nil {
		fmt.Printf(prfx, "failed to auth", err.Error())
		return
	}

	var p plan
	if err := json.NewDecoder(r.Body).Decode(&p); err != nil {
		fmt.Printf(prfx, "json decoding err", p, err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	p.UserID = userID

	if err := p.isValid(); err != nil {
		fmt.Printf(prfx, "validation err", p, err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	
	if err := p.insertToDB(); err != nil {
		fmt.Printf(prfx, "db insert err", p, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusCreated)
}

func edit(w http.ResponseWriter, r *http.Request) {
	prfx := "\nfailed to edit plan: %s, p: %#v, err: %s"

	r.Body = http.MaxBytesReader(w, r.Body, 256)

	userID, err := authorize(w, r.Header.Get("Authorization"))
	if err != nil {
		fmt.Printf(prfx, "failed to auth", nil, err.Error())
		return
	}

	p := plan{
		ID: r.URL.Query().Get("id"),
		UserID: userID,
		Title: r.URL.Query().Get("title"),
	}

	if err := isTitleValid(p.Title); err != nil {
		fmt.Printf(prfx, "validation err", p, err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	if err := p.updateDB(); err != nil {
		fmt.Printf(prfx, "db insert err", p, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)
}

func get(w http.ResponseWriter, r *http.Request) {
	prfx := "\nfailed to get plan: %s, p: %#v, err: %s"

	r.Body = http.MaxBytesReader(w, r.Body, 32)

	userID, err := authorize(w, r.Header.Get("Authorization"))
	if err != nil {
		fmt.Printf(prfx, "failed to auth", nil, err.Error())
		return
	}

	if (r.URL.Query().Get("ids") != "") {
		query(w, r, userID)
		return
	}

	p, err := getFromDB(r.URL.Query().Get("id"))
	if err != nil {
		if err == sql.ErrNoRows {
			w.WriteHeader(http.StatusNoContent)
			return 
		}
		fmt.Printf(prfx, "db get err", p, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if p.UserID != userID {
		fmt.Printf(prfx, "non owner query", p, "user_id: "+userID)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(p); err != nil {
		fmt.Printf(prfx, "json encoding err", p, err)
		w.WriteHeader(http.StatusInternalServerError)
	}	
}

func remove(w http.ResponseWriter, r *http.Request) {
	prfx := "\nfailed to add plan: %s, p: %#v, err: %s"

	r.Body = http.MaxBytesReader(w, r.Body, 32)

	userID, err := authorize(w, r.Header.Get("Authorization"))
	if err != nil {
		fmt.Printf(prfx, "failed to auth", nil, err.Error())
		return
	}

	p, err := getFromDB(r.URL.Query().Get("id"))
	if err != nil {
		fmt.Printf(prfx, "db get err", p, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if p.UserID != userID {
		fmt.Printf(prfx, "non owner deletion", p, "user_id: "+userID)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := p.removeFromDB(); err != nil {
		fmt.Printf(prfx, "db delete err", p, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)
}

func (p *plan) isValid() error {
	if len(p.ID) > 12 || len(p.UserID) > 12{
		return errors.New("id/user_id len greater than 12")
	}
	if err := isTitleValid(p.Title); err != nil {
		return err
	}

	var n = int(time.Now().Unix())
	if p.Updated > n || p.Created > n {
		return errors.New("invalid updated/created dates")
	}

	return nil
}

func query(w http.ResponseWriter, r *http.Request, userID string) {
	prfx := "\nfailed to query plans: %s, userId: %#v, err: %s"

	plans, err := queryDB(r.URL.Query().Get("ids"), userID)
	if err != nil {
		fmt.Printf(prfx, "db get err", userID, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	
	if len(plans) == 0 {
		w.WriteHeader(http.StatusNoContent)
		return 
	}
	
	w.Header().Add("Content-type", "application/json")
	w.WriteHeader(http.StatusOK)
	if err := json.NewEncoder(w).Encode(plans); err != nil {
		fmt.Printf(prfx, "json encoding err", userID, err)
		w.WriteHeader(http.StatusInternalServerError)
	}
}

func queryDB(ids, userID string) ([]plan, error) {
	plans := []plan{}

	idsList := []interface{}{userID}
	for _, v := range strings.Split(ids, ",") {
		idsList = append(idsList, v)
	}

	params := ""
	for i:=2;i<len(idsList)+1;i++ {
		params = params+"$"+strconv.Itoa(i)+","
	}

	params = strings.TrimSuffix(params, ",")

	q := "SELECT id, user_id, title, updated, "+ 
		"created FROM "+dbName+" WHERE user_id=$1 AND id NOT IN ("+params+")"

	rows, err := db.Query(q, idsList...)
	if err != nil {
		return plans, err
	}
	defer rows.Close()

	for rows.Next() {
		p := plan{}
		err := rows.Scan(&p.ID, &p.UserID, &p.Title, &p.Updated, &p.Created)
		if err != nil {
			return plans, err
		}

		plans = append(plans, p)
	}
	if err := rows.Err(); err != nil {
		return plans, err;
	}

	return plans, err
}

func isTitleValid(t string) error {
	if len(t) == 0 || len(t) > 100 {
		return errors.New("title len greater than 100")
	}
	return nil
}

func (p *plan) insertToDB() error {
	_, err := db.Query("INSERT INTO "+dbName+" (id, user_id, title, updated, "+
		"created) VALUES ($1, $2, $3, $4, $5)", p.ID, p.UserID, p.Title,
		p.Updated, p.Created)
	return err
}

func (p *plan) updateDB() error {
	_, err := db.Query("UPDATE "+dbName+" SET title=$1 WHERE id=$2 AND user_id=$3", 
		p.Title, p.ID, p.UserID)
	return err
}

func (p *plan) removeFromDB() error {
	_, err := db.Query("DELETE FROM "+dbName+" WHERE id = $1", p.ID)
	return err
}

func getFromDB(id string) (plan, error) {
	row := db.QueryRow("SELECT user_id, title, updated, "+ 
		"created FROM "+dbName+" WHERE id = $1", id)

	p := plan{ID: id}
	err := row.Scan(&p.UserID, &p.Title, &p.Updated, &p.Created)

	return p, err
}

func authorize(w http.ResponseWriter, tkn string) (string, error) {
	req, err := http.NewRequest("GET", "http://localhost:8080/auth", nil)
	if err != nil {
		return "", fmt.Errorf("failed to create req: %s", err.Error())
	}
	req.Header.Set("Authorization", tkn)

	res, err := (&http.Client{}).Do(req)
	if err != nil {
		return "", fmt.Errorf("failed to execute req: %s", err.Error())
	}

	switch res.StatusCode {
	case 200:
		break
	case 403:
		w.WriteHeader(http.StatusForbidden)
		return "", fmt.Errorf("req returned status: 403")
	default:
		w.WriteHeader(http.StatusInternalServerError)
		return "", fmt.Errorf("req returned status: %d", res.StatusCode)
	}
	
	userID, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", fmt.Errorf("failed to read body: %s", err.Error())
	}

	return string(userID), nil
}

func httpCORS(w http.ResponseWriter, r *http.Request) bool {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	if r.Method == http.MethodOptions {
		w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		w.Header().Set("Access-Control-Max-Age", "3600")
		w.WriteHeader(http.StatusOK)
		return true;
	}

	return false
}