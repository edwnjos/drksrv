package main

import (
	"database/sql"
	"io/ioutil"
	"log"
	"net/http"

	_ "github.com/lib/pq"
	"gitlab.com/edwnjos/drksrv/analytics"
	"gitlab.com/edwnjos/drksrv/auth"
	"gitlab.com/edwnjos/drksrv/mailinglist"
	"gitlab.com/edwnjos/drksrv/plans"
	"gitlab.com/edwnjos/drksrv/profile"
	"gitlab.com/edwnjos/drksrv/workoutlogs"
	"gitlab.com/edwnjos/drksrv/workouts"
)

func main() {
	// getting and passing db conn to services..
	c, err := ioutil.ReadFile("build/db/conn")
	if err != nil {
		log.Fatal(err)
	}
	db, err := sql.Open("postgres", string(c))
	if err != nil {
		log.Fatal(err)
	}
	auth.SetDB(db)
	profile.SetDB(db)
	plans.SetDB(db)
	workouts.SetDB(db)
	analytics.SetDB(db)
	mailinglist.SetDB(db)
	workoutlogs.SetDB(db)

	// getting and passing email key to services..
	emailKey, err := ioutil.ReadFile("build/emailkey")
	if err != nil {
		log.Fatal(err)
	}
	auth.SetEmailKey(string(emailKey))

	http.HandleFunc("/auth", auth.Authorize)
	http.HandleFunc("/auth/signin", auth.SignIn)
	http.HandleFunc("/auth/verify", auth.Authenticate)
	http.HandleFunc("/profile", profile.HTTPRouter)
	http.HandleFunc("/workouts", workouts.HTTPRouter)
	http.HandleFunc("/plans", plans.HTTPRouter)
	http.HandleFunc("/analytics", analytics.HTTPRouter)
	http.HandleFunc("/mailinglist", mailinglist.HTTPRouter)
	http.HandleFunc("/workoutlogs", workoutlogs.HTTPRouter)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
