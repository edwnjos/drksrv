package analytics

import (
	"net/http"
	"time"
	"encoding/json"
	"crypto/rand"
	"encoding/hex"

	"database/sql"
	"fmt"
)

type log struct {
	ID string
	Err string
	Created int
}

var db *sql.DB

const dbName = "public.analytics"

// SetDB passes db connection
func SetDB(d *sql.DB) {db = d}

// HTTPRouter routes requests
func HTTPRouter(w http.ResponseWriter, r *http.Request) {
	if option := httpCORS(w, r); option {return}

	switch r.Method {
	case http.MethodPost:
		report(w, r)
		break
	}
}

func report(w http.ResponseWriter, r *http.Request) {
	prfx := "\nfailed to report analytics: %s, l: %#v, err: %s"

	var l log
	if err := json.NewDecoder(r.Body).Decode(&l); err != nil {
		fmt.Printf(prfx, "json decoding err", l, err.Error())
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	id, err := generateID(12)
	if err != nil {
		fmt.Printf(prfx, "gen id err", l, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	l.ID = id

	l.Created = int(time.Now().Unix())

	if err := l.insertToDB(); err != nil {
		fmt.Printf(prfx, "db insert err", l, err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (l *log) insertToDB() error {
	_, err := db.Query("INSERT INTO "+dbName+" (id, err, created) VALUES ($1, $2, $3)", 
		l.ID, l.Err, l.Created)
	return err
}

func httpCORS(w http.ResponseWriter, r *http.Request) bool {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	if r.Method == http.MethodOptions {
		w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		w.Header().Set("Access-Control-Max-Age", "3600")
		w.WriteHeader(http.StatusOK)
		return true;
	}

	return false
}

func generateID(len int) (string, error) {
	b := make([]byte, int(len/2))
	if _, err := rand.Read(b); err != nil {
		return "", err
	}
	return hex.EncodeToString(b), nil
}