CREATE TABLE public.analytics
(
    id character varying(16) COLLATE pg_catalog."default" NOT NULL,
    err text COLLATE pg_catalog."default" NOT NULL,
    created integer NOT NULL,
    CONSTRAINT analytics_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.analytics
    OWNER to postgres;
