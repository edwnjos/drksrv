package auth

import (
	"crypto/rand"
	"crypto/sha256"
	"database/sql"
	"encoding/hex"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/edwnjos/mail"
	"gitlab.com/edwnjos/drksrv/profile"
	"golang.org/x/crypto/pbkdf2"
)

type auth struct {
	ID            string
	UserID        string
	Code          string
	Attempts      int
	Token         string
	Authenticated int64
	Created       int64
}

var db *sql.DB

var emailKey string

const dbName = "public.authorizations"

// SetDB passes db connection
func SetDB(d *sql.DB) { db = d }

// SetEmailKey passes email key
func SetEmailKey(k string) { emailKey = k }

// SignIn ccreate new auth and sends code to user email
func SignIn(w http.ResponseWriter, r *http.Request) {
	prfx := "\nfailed to signin: %s, err: %s\n"

	if option := httpCORS(w, r); option {
		return
	}

	em := r.URL.Query().Get("e")

	code, err := generateID(8)
	if err != nil {
		fmt.Printf(prfx, "code gen err", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	au := auth{
		Code:    strings.ToUpper(code),
		Created: time.Now().Unix(),
	}

	au.UserID, err = profile.GetID(em)
	if err != nil {
		fmt.Printf(prfx, "get with email err", err.Error())
		if err.Error() == "not_found" {
			w.WriteHeader(http.StatusNoContent)
			return
		}
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := au.insertToDB(); err != nil {
		fmt.Printf(prfx, "insert db err", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := au.sendMail(em); err != nil {
		fmt.Printf(prfx, "failed to send mail", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(au.ID))
}

// Authenticate verifies login code and returns token
func Authenticate(w http.ResponseWriter, r *http.Request) {
	prfx := "\nfailed to signin auth: %s, err: %#v\n"

	if option := httpCORS(w, r); option {
		return
	}

	au := auth{ID: r.URL.Query().Get("id")}
	if err := au.getFromDB(); err != nil {
		fmt.Printf(prfx, "query err", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if err := au.addAttempt(); err != nil {
		fmt.Printf(prfx, "add attempt failed", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if au.Authenticated != 0 {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	if au.Attempts >= 10 {
		w.WriteHeader(http.StatusTooManyRequests)
		return
	}

	c := r.URL.Query().Get("c")
	if au.Code != c {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	if time.Now().Unix() > au.Created+600 { //10m
		w.WriteHeader(http.StatusRequestTimeout)
		return
	}

	if err := au.setAuthenticated(); err != nil {
		fmt.Printf(prfx, "set auth failed", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(au.Token))
}

// Authorize verifies user key
func Authorize(w http.ResponseWriter, r *http.Request) {
	prfx := "\nfailed to authorize: %s, err: %#v\n"

	if option := httpCORS(w, r); option {
		return
	}

	id, token, err := decodeToken(r.Header.Get("Authorization"))
	if err != nil {
		fmt.Printf(prfx, "failed decode authorization", err)
		w.WriteHeader(http.StatusForbidden)
		return
	}

	au := auth{ID: id}
	if err := au.getFromDB(); err != nil {
		fmt.Printf(prfx, "query err", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	if au.Token != hash(token, au.Code) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	if au.Authenticated == 0 {
		fmt.Printf(prfx, "unauth token used", au)
		w.WriteHeader(http.StatusForbidden)
		return
	}

	w.Header().Add("Content-type", "text/plain; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(au.UserID))
}

func httpCORS(w http.ResponseWriter, r *http.Request) bool {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	if r.Method == http.MethodOptions {
		w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE")
		w.Header().Set("Access-Control-Allow-Headers", "*")
		w.Header().Set("Access-Control-Max-Age", "3600")
		w.WriteHeader(http.StatusOK)
		return true
	}

	return false
}

func (au *auth) sendMail(e string) error {
	print("send mail called")
	b := "<html><p>Here's your login code: " + au.Code + "</p><br></html>"
	m := mail.Mail{
		To:      mail.Acct{Name: "DarkPlace User", Addr: e},
		From:    mail.Acct{Name: "DarkPlace Team", Addr: "hello@darkplace.app"},
		Subject: "Log in to DarkPlace",
		Body:    []byte(b),
		Client:  mail.SendgridClient,
		AuthKey: emailKey,
	}

	return m.Send()
}

func (au *auth) setAuthenticated() error {
	var err error

	au.Token, err = generateID(28)
	if err != nil {
		return fmt.Errorf("id gen err: %s", err)
	}

	_, err = db.Query("UPDATE "+dbName+" SET token=$1, authenticated=$2 WHERE id=$3",
		hash(au.Token, au.Code), time.Now().Unix(), au.ID)

	au.Token = fmt.Sprintf("%s.%s", au.ID, au.Token)

	return err
}

func hash(p, s string) string {
	return hex.EncodeToString(pbkdf2.Key([]byte(p), []byte(s), 20000, 32, sha256.New))
}

func decodeToken(t string) (string, string, error) {
	res := strings.Split(t, ".")
	if len(res) != 2 {
		return "", "", fmt.Errorf("invalid token: %s", t)
	}
	return res[0], res[1], nil
}

func (au *auth) addAttempt() error {
	au.Attempts = au.Attempts + 1
	_, err := db.Query("UPDATE "+dbName+" SET attempts=$1 WHERE id=$2", au.Attempts, au.ID)
	return err
}

func (au *auth) insertToDB() error {
	id, err := generateID(12)
	if err != nil {
		return fmt.Errorf("id gen err: %s", err)
	}
	au.ID = id

	_, err = db.Query("INSERT INTO "+dbName+" (id, user_id, code, attempts, token, "+
		"authenticated, created) VALUES ($1, $2, $3, $4, $5, $6, $7)", au.ID, au.UserID,
		au.Code, au.Attempts, au.Token, au.Authenticated, au.Created,
	)
	return err
}

func (au *auth) getFromDB() error {
	row := db.QueryRow("SELECT user_id, code, attempts, token, authenticated, "+
		"created FROM "+dbName+" WHERE id = $1", au.ID)

	return row.Scan(&au.UserID, &au.Code, &au.Attempts,
		&au.Token, &au.Authenticated, &au.Created)
}

func generateID(len int) (string, error) {
	b := make([]byte, int(len/2))
	if _, err := rand.Read(b); err != nil {
		return "", err
	}
	return hex.EncodeToString(b), nil
}
