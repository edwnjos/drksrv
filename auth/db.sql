CREATE TABLE public.authorizations
(
    id character varying(12) COLLATE pg_catalog."default" NOT NULL,
    user_id character varying(12) COLLATE pg_catalog."default" NOT NULL,
    code character varying(12) COLLATE pg_catalog."default" NOT NULL,
    attempts smallint NOT NULL,
    token text COLLATE pg_catalog."default" NOT NULL,
    authenticated integer,
    created integer,
    CONSTRAINT authorizations_pkey PRIMARY KEY (id)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.authorizations
    OWNER to postgres;